package PT2017.Homework2.Control;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import PT2017.Homework2.Model.Manager;
import PT2017.Homework2.View.QueView;
import PT2017.Homework2.View.WelcomeGUI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
/*
 * 
 * Controller class glueing together the views and the models
 */
public class Controller implements EventHandler<ActionEvent>{

	private WelcomeGUI welcomeFace;
	private Manager manager;
	private int minInterval;
	private int maxInterval;
	private int minServiceTime;
	private int maxServiceTime;
	private int startTime;
	private int finishingTime;
	private int nrOfQues;
	
	public Controller(WelcomeGUI welcomeFace){
		this.welcomeFace = welcomeFace;
		welcomeFace.minInterval.setOnAction(this);
		welcomeFace.maxInterval.setOnAction(this);
		welcomeFace.minServiceTime.setOnAction(this);
		welcomeFace.maxServiceTime.setOnAction(this);
		welcomeFace.nrOfQues.setOnAction(this);
		welcomeFace.startTime.setOnAction(this);
		welcomeFace.finishingTime.setOnAction(this);
		welcomeFace.start.setOnAction(this);
		
	}
	
	public void handle(ActionEvent event){
		//check the button
		if(event.getSource() == welcomeFace.start){

			minInterval = Integer.parseInt(welcomeFace.minInterval.getText());
		
			maxInterval = Integer.parseInt(welcomeFace.maxInterval.getText());
		
			minServiceTime = Integer.parseInt(welcomeFace.minServiceTime.getText());
		
			maxServiceTime = Integer.parseInt(welcomeFace.maxServiceTime.getText());
		
			startTime = Integer.parseInt(welcomeFace.startTime.getText());
		
			finishingTime = Integer.parseInt(welcomeFace.finishingTime.getText());
		
			nrOfQues = Integer.parseInt(welcomeFace.nrOfQues.getText());
		
			//check for every field if their value is correct
			boolean createManager = true;
			if(welcomeFace.minInterval.getText().equals("")){
				welcomeFace.errorLabel.setText("ERROR!\nNo value stated for minimum\ninterval between clients");
				createManager = false;
			}
			if(minInterval>maxInterval){
				welcomeFace.errorLabel.setText("ERROR!\nInvalid value for the maximum\ntime between clients");
				createManager = false;
			}
			if(welcomeFace.minServiceTime.getText().equals("") || minServiceTime<=0){
				welcomeFace.errorLabel.setText("ERROR!\nInvalid value for the minimum\nservice time");
				createManager = false;
			}
			if(welcomeFace.maxServiceTime.getText().equals("") || maxServiceTime<=0 || minServiceTime > maxServiceTime){
				welcomeFace.errorLabel.setText("ERROR!\nInvalid value for the maximum\nservice time");
				createManager = false;
			}
			if(welcomeFace.startTime.getText().equals("")){
				welcomeFace.errorLabel.setText("ERROR!\nInvalid value for the starting\ntime");
				createManager = false;
			}
			if(welcomeFace.startTime.getText().equals("") || startTime > finishingTime ){			
				welcomeFace.errorLabel.setText("ERROR!\nInvalid value for the finishing\ntime");
				createManager = false;
			}
			
			if(createManager){
				
				final ScheduledExecutorService executer =Executors.newScheduledThreadPool(nrOfQues); 
				
				this.manager = new Manager(nrOfQues,minInterval,maxInterval,minServiceTime,maxServiceTime,startTime,finishingTime);
				QueView queView = new QueView(manager.getQues(),startTime,finishingTime,manager);
				Stage primaryStage = new Stage();
				primaryStage.setScene(queView.scene);
				primaryStage.show();
				
				executer.scheduleAtFixedRate(queView, 0, 1, TimeUnit.SECONDS);
				executer.scheduleAtFixedRate(manager, 0,1,TimeUnit.SECONDS);
				Runnable runable = new Runnable(){
					public void run(){
						//manager.shutDownExecutorService();
						executer.shutdownNow();
					}
				};
				executer.schedule(runable, finishingTime, TimeUnit.SECONDS);
				welcomeFace.errorLabel.setText("");
			}
		}
	}
	
	
}
