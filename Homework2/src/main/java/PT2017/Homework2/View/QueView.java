package PT2017.Homework2.View;

import PT2017.Homework2.Model.*;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;

/**
 * This view will display the evolution of the queues
 * @author Manii
 *
 */

public class QueView implements Runnable{
	
	public int queX = 100;
	public int queY = 200;
	public int registerX = 800;
	public int clientY  = 20;
	public int currentTime;
	public int endTime;
	public Pane pane;
	public Scene scene;
	public Stage stage = new Stage();
	public List<Label> ques = new ArrayList<Label>();
	public List<Label> clients = new ArrayList<Label>();
	public List<Queue> que;
	public Label time = new Label();
	public Manager manager;
	public Label peakSecond;
	public Label averageWaitingTime;
	public Label averageServiceTime;
	public List<Label> queInfo = new ArrayList<Label>();
	public TextArea logger;
	
	public QueView(List<Queue> que,int startTime,int endTime,Manager manager){
		pane = new Pane();
		scene = new Scene(pane, 1500, 800);		
		this.que = que;
		this.endTime = endTime;
		this.currentTime = startTime;
		time.setLayoutX(100);
		time.setLayoutY(100);
		this.manager = manager;
		logger = new TextArea();
		logger.setLayoutX(100);
		logger.setLayoutY(550);
		logger.setStyle("-fx-font-weight: bold");
		logger.setPrefSize( 1300,200);
		pane.getChildren().add(logger);
		pane.setStyle("-fx-background-color:" + "#49A682");

	}

	
	private void removeErrithing(){
		pane.getChildren().removeAll(ques);
		pane.getChildren().removeAll(clients);
		pane.getChildren().remove(time);
	}

	public void run() {
		
		
			System.out.println("View time "+currentTime);
			Thread runnable =new Thread(){
			public void run(){
				if(currentTime <=endTime){
					System.out.println("View time1 "+currentTime);
					removeErrithing();
					time.setText("Current Time: " + currentTime);
					pane.getChildren().add(time);
					for(Queue queue:que){
						//draw label for que
						Label newQLabel = new Label("Register");
						newQLabel.setLayoutX(queX);
						newQLabel.setLayoutY(queY);
						//newQLabel.setPrefSize(50, 50);
						//newQLabel.setGraphic(reg);
						ques.add(newQLabel);
						pane.getChildren().add(newQLabel);
						for(Client client:queue.getClients()){
							Label newCLabel = new Label(client.toString());//client.toString()
							newCLabel.setLayoutX(queX);
							newCLabel.setLayoutY(queY+clientY);
							//newCLabel.setPrefSize(25, 25);
							//newCLabel.setGraphic(cust);
							clients.add(newCLabel);
							clientY+=20;
							pane.getChildren().add(newCLabel);
						}
						clientY = 10;
						queX += 100;
						//update the textField;
						logger.appendText(queue.getAddMessege());
						logger.appendText(queue.getRemoveMessege());
						
					}
					queX = 100;
					currentTime++;
				}
				else{
					//here we create the statistics based on values computed by the manager
					//display the label containing the peakSecond
					peakSecond = new Label();
					peakSecond.setLayoutX(800);
					peakSecond.setLayoutY(100);
					peakSecond.setText("Peak Time: "+manager.getPeakTime());
					pane.getChildren().add(peakSecond);
					//display the average waiting time
					averageWaitingTime = new Label();
					averageWaitingTime.setLayoutX(800);
					averageWaitingTime.setLayoutY(130);
					averageWaitingTime.setText("Average waiting time in the shop: "+ manager.getAverageWaitingTime());
					pane.getChildren().add(averageWaitingTime);
					//display the average service time
					averageServiceTime = new Label();
					averageServiceTime.setLayoutX(800);
					averageServiceTime.setLayoutY(160);
					averageServiceTime.setText("Average service time in the shop: "+ manager.getAverageServiceTime());
					pane.getChildren().add(averageServiceTime);
					//display information about the ques
					for(Queue ques: que){
						int a = ques.getQueId()+1;
						Label newQLabel = new Label("Register "+ a +"\nQue was empty " + ques.getEmptyQueTime()+" seconds"+
										"\nTotal Clients "+ ques.clientCount()+"\nProcessed Clients "+ques.processedClientCount());
						newQLabel.setLayoutX(registerX);
						newQLabel.setLayoutY(200);
						pane.getChildren().add(newQLabel);
						registerX+=150;					
					}
				}
			}
		};
		
		Platform.runLater(runnable);		
		
	}
}
	

