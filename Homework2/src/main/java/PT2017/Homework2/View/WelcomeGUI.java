package PT2017.Homework2.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class WelcomeGUI {

	public Scene scene;
	public Pane pane;
	public Button start;
	public Label l1;
	public Label l2;
	public Label l3;
	public TextField minInterval;
	public TextField maxInterval;
	public TextField minServiceTime;
	public TextField maxServiceTime;
	public Label l4;
	public TextField startTime;
	public TextField finishingTime;
	public TextField nrOfQues;
	public Label l5;
	public Label errorLabel;
	
	public Scene getScene(){
		return scene;
	}
	
	public WelcomeGUI(){
		   pane = new Pane();
		   scene = new Scene(pane, 500, 800);
		   
		   l2 = new Label("			    Greetings!\nPlease introduce the values(seconds) into the fields.");
		   l2.setLayoutX(95);
		   l2.setLayoutY(100);
		   l2.setStyle("-fx-font: 15 Sherif");
		   pane.getChildren().add(l2);
		   
		   //setup the interface to set the interval between clients
		   l1 = new Label("Enter the interval between customers");
		   l1.setLayoutX(150);
		   l1.setLayoutY(200);
		   l1.setStyle("-fx-font-weight: bold");
		   pane.getChildren().add(l1);
		   
		   minInterval = new TextField();
		   minInterval.setPromptText("min");
		   minInterval.setLayoutX(190);
		   minInterval.setLayoutY(225);
		   minInterval.setPrefSize(50, 20);
		   pane.getChildren().add(minInterval);
		   

		   maxInterval = new TextField();
		   maxInterval.setPromptText("max");
		   maxInterval.setLayoutX(250);
		   maxInterval.setLayoutY(225);
		   maxInterval.setPrefSize(50, 20);
		   pane.getChildren().add(maxInterval);
		   
		   //setup the interface to set the minimum and maximum service times
		   l3 = new Label("Enter the interval of service");
		   l3.setLayoutX(170);
		   l3.setLayoutY(270);
		   l3.setStyle("-fx-font-weight: bold");
		   pane.getChildren().add(l3);
		   
		   minServiceTime= new TextField();
		   minServiceTime.setPromptText("min");
		   minServiceTime.setLayoutX(190);
		   minServiceTime.setLayoutY(295);
		   minServiceTime.setPrefSize(50, 20);
		   pane.getChildren().add(minServiceTime);
		   

		   maxServiceTime = new TextField();
		   maxServiceTime.setPromptText("max");
		   maxServiceTime.setLayoutX(250);
		   maxServiceTime.setLayoutY(295);
		   maxServiceTime.setPrefSize(50, 20);
		   pane.getChildren().add(maxServiceTime);
		   
		   //setup the interval of simulation
		   l4 = new Label("Enter the interval of simulation");
		   l4.setLayoutX(160);
		   l4.setLayoutY(340);
		   l4.setStyle("-fx-font-weight: bold");
		   pane.getChildren().add(l4);
		   
		   startTime= new TextField();
		   startTime.setPromptText("min");
		   startTime.setLayoutX(190);
		   startTime.setLayoutY(365);
		   startTime.setPrefSize(50, 20);
		   pane.getChildren().add(startTime);
		   

		   finishingTime= new TextField();
		   finishingTime.setPromptText("max");
		   finishingTime.setLayoutX(250);
		   finishingTime.setLayoutY(365);
		   finishingTime.setPrefSize(50, 20);
		   pane.getChildren().add(finishingTime);
		   
		   //setup the number of queues
		   l5 = new Label("Enter the number of queues");
		   l5.setLayoutX(170);
		   l5.setLayoutY(410);
		   l5.setStyle("-fx-font-weight: bold");
		   pane.getChildren().add(l5);
		   
		   nrOfQues= new TextField();
		   nrOfQues.setPromptText("nr");
		   nrOfQues.setLayoutX(222);
		   nrOfQues.setLayoutY(430);
		   nrOfQues.setPrefSize(50, 20);
		   pane.getChildren().add(nrOfQues);
		   
		   //start button
		   start = new Button("Start Simulation");
		   start.setLayoutX(200);
		   start.setLayoutY(500);
		   pane.getChildren().add(start);
		   
		   //error label
		   errorLabel = new Label();
		   errorLabel.setLayoutX(150);
		   errorLabel.setLayoutY(600);
		   errorLabel.setTextFill(Color.web("#ecf0f1"));
		   pane.getChildren().add(errorLabel);
		   
		   pane.setStyle("-fx-background-color:" + "#49A682");
		   
	}
}
