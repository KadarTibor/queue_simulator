package PT2017.Homework2.Model;
/**
 * This Class models the Cleint behaviour
 * -in our case essential information about a client:
 * 	--arrival time
 * 	--service time required
 * 	--finish time(should be ideally arrival time + service time required)
 * @author Manii
 *
 */
public class Client {
	
	private int clientId;
	private int arrivalTime;
   	private int serviceTime;
   	private int counter;
   	private int finishTime;
   
    public Client(int clientId,int arrivalTime,int serviceTime){
	   this.clientId = clientId;
	   this.arrivalTime = arrivalTime;
	   this.serviceTime = serviceTime;
	   this.counter = serviceTime;
    }
    public int getCounter(){
    	return counter;
    }
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}
	
	public int getServiceTime() {
		return serviceTime;
	}
	
	public void setServiceTime(int serviceTime){
		this.serviceTime = serviceTime;
	}
	
	public int getFinishTime() {
		return finishTime;
	}
	
	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}
	@Override
	public String toString(){
		return String.format("C%d(%d, %d)",clientId,arrivalTime,serviceTime);
	}
   
   
}
