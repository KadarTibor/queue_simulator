package PT2017.Homework2.Model;

import java.util.Random;
/**
 * This class will model the behavior of a client generator
 * tasks needed to be modeled:
 * 		--create new Client ( arrivalTime and serviceTime )
 * 		--generate the clients during the time simulation
 * 	the class requires as input:
 * 	- Minimum and maximum interval of arriving time between clients;
 *	- Minimum and maximum service time;
 * @author Manii
 *
 */
public class ClientGenerator {
	
	private int intervalBetweenClients;
	private int serviceTime;
	private int minServiceTime;
	private int clientId;
	
	public ClientGenerator(int minInterval, int maxInterval, int minServiceTime, int maxServiceTime ){
		this.minServiceTime = minServiceTime;
		this.intervalBetweenClients = maxInterval -  minInterval;
		this.serviceTime = maxServiceTime - minServiceTime;
	}
	/*
	 *this method will return either a valid client or a client with arrival time 0 and service time 0 
	 *caller has to check veridicity of the client 
	 */
	public Client generate(int currentTime){
		//we need random arrival time in the min-max interval
		
			Random random = new Random();
			int arrivalTime = currentTime + random.nextInt(intervalBetweenClients)+1;
			//get a random service time
			int servTime =minServiceTime+ random.nextInt(serviceTime)+1;
			return new Client(++clientId,arrivalTime,servTime);
		
	}
}
