package PT2017.Homework2.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class Manager implements Runnable {

	private List<Queue> ques = new ArrayList<Queue>(); 
	private int minIntervalBetweenClients;
	private int maxIntervalBetweenClients;
	private int endTime;
	private int currentTime;
	private boolean clientBuilt;
	private ClientGenerator generator;
	private ExecutorService executer;
	private Client newClient;
	private int peakTime;
	private int maxClients;
	private int sumPerSecond;
	private int averageServiceTime;
	private int clientCount;
	
	
	public void shutDownExecutorService(){
		executer.shutdown();
	}
	public Manager(int nrOfQueues, int minInterval, int maxInterval, int minServiceTime, int maxServiceTime,int startTime, int endTime){
		
		//set up the queues
		for(int i = 0; i < nrOfQueues; i++){
					ques.add(new Queue(i));
		}
		
		this.minIntervalBetweenClients = minInterval;
		this.maxIntervalBetweenClients = maxInterval;
		this.endTime = endTime;
		this.currentTime = startTime;
		this.clientBuilt = false;
		this.generator = new ClientGenerator(minIntervalBetweenClients,maxIntervalBetweenClients,minServiceTime,maxServiceTime);
		this.executer = Executors.newFixedThreadPool(nrOfQueues);
	}
	
	public List<Queue> getQues(){
		return ques;
	}
	
	public void run() {
		if(currentTime<=endTime){
			sumPerSecond = 0;
			System.out.println("Current simulation time: "+ currentTime);
			//only generate a client if the time has come
			if(!clientBuilt){
				newClient = generator.generate(currentTime);
				averageServiceTime += newClient.getServiceTime();
				clientCount++;
				clientBuilt = true;
			}
			//only introduce the client if it arrived
			if(clientBuilt && newClient.getArrivalTime()==currentTime){
			
				//get the smallest waiting time from the ques
				int min = ques.get(0).getWaitTimeNow();
				int index = 0;
				for(Queue que:ques){
					if(que.getWaitTimeNow()<min){
						min = que.getWaitTimeNow();
						index = ques.indexOf(que);
					}
				
				}
				
				
				//place it in the queue
				System.out.println("clien "+newClient.getClientId()+" added to que nr " + ques.get(index).getQueId());
				ques.get(index).addClient(newClient);
				clientBuilt = false;
				
			}
			for(Queue que: ques){
				sumPerSecond += que.clientCounter();
			}
			if(sumPerSecond>maxClients){
				maxClients = sumPerSecond;
				peakTime = currentTime;
			}
			currentTime+=1;
			for(Queue que: ques){
				executer.execute(que);
			}
			
		
		
		}
	}
	//method to return the peakTime
	public int getPeakTime(){
		return peakTime;
	}
	
	//method to return the average serviceTime
	public float getAverageServiceTime(){
		int totalClients = 0;
		int totalServiceTime = 0;
		for(Queue que:ques){
			totalClients += que.processedClientCount();
			totalServiceTime +=que.getServiceTimeOfProcessedClients();
		}
		System.out.println("---weeon"+totalClients+"--weeon"+totalServiceTime);
		return totalServiceTime/(float)totalClients;
	}
	//method to return the average waiting time per all ques
	public float getAverageWaitingTime(){
		int totalTimeWaiting = 0;
		int nrOfClientsServed = 0;
		for(Queue que:ques){
			totalTimeWaiting += que.getWaitTime();
			System.out.println("the wait timeis "+que.getWaitTime());
			nrOfClientsServed+=que.processedClientCount();
		}
		return totalTimeWaiting/(float)nrOfClientsServed;
	}
	
}
