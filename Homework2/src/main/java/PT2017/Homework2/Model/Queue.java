package PT2017.Homework2.Model;

import java.util.ArrayList;
import java.util.List;

public class Queue implements Runnable {
	private int queId;
	private int emptyQueTime;
	private int cCount;
	private String addMessege;
	private String removeMessege;
	private List<Client> clients = new ArrayList<Client>();	//list of clients currently in the queue
	private List<Client> processedClients = new ArrayList<Client>();	//list of clients processed by the queue
	private int waitTime;//total wait time of the Queue
	
	//constructor which sets an identifier for the queue
	public Queue(int queId){
		this.queId = queId;
		waitTime = 0;
		addMessege = "";
		removeMessege = "";
	}
	
	public void addClient(Client newClient){
		System.out.print(newClient);
		addMessege ="\nClient " + newClient.getClientId() + " arrived at " + newClient.getArrivalTime() +" needs "+ newClient.getServiceTime() +" seconds service and is added to que nr " + queId+".";
		clients.add(newClient);
		cCount++;
	}
	//method to retrieve the log
	public String getAddMessege(){
		String a = addMessege;
		addMessege = "";
		return a;
	}
	public int getServiceTimeOfProcessedClients(){
		int a = 0;
		for(Client client:processedClients){
			a += client.getCounter();
		}
		return a;
	}
	//method to retrieve the remove log
	public String getRemoveMessege(){
		String a = removeMessege;
		removeMessege = "";
		return a;
	}
	//method to retrieve the list of waiting clients
	public List<Client> getClients(){
		return clients;
	}
	//method to retrieve the nr of waiting clients
	public int clientCount(){
		return cCount;
	}
	public int clientCounter(){
		return clients.size();
	}
	//method to retrieve the nr of processed clients
	public int processedClientCount(){
		return processedClients.size();
	}
	///method to retrieve the clients which were served
	public List<Client> getServedClients(){
		return processedClients;
	}
	//method that calculates in a certain moment the wait time
	public int getWaitTime(){
		waitTime = 0;
		for(Client client:processedClients){
			waitTime += client.getFinishTime();
		}
		return waitTime;
	}
	
	//method to get the momentary wait time
	public int getWaitTimeNow(){
		waitTime = 0;
		for(Client client:clients){
			waitTime += client.getFinishTime();
		}
		return waitTime;
	}
	//method that serves a client
	public void serveClient(int time){
		//decrease the time associated to the client
		if(!clients.isEmpty()){
			Client first = clients.get(0);
			first.setServiceTime(first.getServiceTime() - time);
			//if it is served remove him from the list
			if(first.getServiceTime()<=0){
				System.out.println("---removed client from the que" + getQueId());
				processedClients.add(first);
				int a = first.getArrivalTime()+first.getFinishTime();
				removeMessege ="\nClient " + first.getClientId() + " left at " + a +" from que nr " + queId+".";
				clients.remove(0);
			}
		}
	
	
	}
	//getter for the emptyQueTime
	public int getEmptyQueTime(){
		return emptyQueTime;
	}
	//update the waiting time of each client
	public void updateWaitTime(int time){
		for(Client client:clients){
			client.setFinishTime(client.getFinishTime()+time);
		}
	}
	//method to be called when thread starts
	public void run() {
			if(!clients.isEmpty()){
				System.out.println("Que "+ getQueId() +" is not empty ");
				updateWaitTime(1);
				serveClient(1);
				
			}
			else
				 emptyQueTime++;
		
			
	}
	
	public int getQueId(){
		return queId;
	}
	
}
