package PT2017.Homework2.Test;

import PT2017.Homework2.Control.Controller;
import PT2017.Homework2.View.WelcomeGUI;
import javafx.application.Application;
import javafx.stage.Stage;

public class GuiTest extends Application{

	public static void main(String[] args){
		launch();
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Queue advisor");
		primaryStage.setResizable(false);
		WelcomeGUI welcomeFace = new WelcomeGUI();
		Controller controller = new Controller(welcomeFace);
		primaryStage.setScene(welcomeFace.getScene());
		primaryStage.show();
		
	}

}
